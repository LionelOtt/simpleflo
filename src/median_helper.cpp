#include <stdexcept>

#include <median_helper.h>


MedianHelper::MedianHelper(double scale, double variance)
    :   m_rng_ready(false)
      , m_scale(scale)
      , m_variance(variance)
      , m_rng_generator()
      , m_rng_distribution(0.0, 1.0)
{}

void MedianHelper::set_params(double scale, double variance)
{
    m_scale = scale;
    m_variance = variance;
    m_rng_ready = false;
}

void MedianHelper::init_rng()
{
    double median = boost::accumulators::median(m_accumulator);
    if(!std::isfinite(median))
    {
        throw std::runtime_error("Invalid median value");
    }

    if(std::abs(median) - 0.1 < 0.0)
    {
        m_rng_distribution = Distribution_t(-0.1 * m_scale, 0.1 * m_scale);
    }
    else
    {
        m_rng_distribution = Distribution_t(
                (m_scale * median) - m_variance*std::abs(median),
                (m_scale * median) + m_variance*std::abs(median)
        );
    }

    m_rng = ValueGeneratorPtr_t(
            new ValueGenerator_t(m_rng_generator, m_rng_distribution)
    );
    m_rng_ready = true;
}
