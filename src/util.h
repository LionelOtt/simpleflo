#ifndef __UTIL_H__
#define __UTIL_H__


#include <functional>
#include <string>
#include <vector>

#include <kobold/gaussian_dataset.hpp>

#include <Eigen/Core>

#include <facility_location_outliers.h>


/**
 * \brief Computes the euclidean distance between two vectors.
 *
 * \param a first vector
 * \oaram b second vector
 * \return distance between the two vectors
 */
double euclidean_distance(Eigen::VectorXd const& a, Eigen::VectorXd const& b);

/**
 * \brief Computes the shortes angular distance between two vectors.
 *
 * \param a first vector
 * \oaram b second vector
 * \return distance between the two vectors
 */
double angular_distance(Eigen::VectorXd const& a, Eigen::VectorXd const& b);

/**
 * \brief Writes clustering results to disk for visualization.
 *
 * The format is as follows:
 * <exemplar id> <coord 1> ... <coord N>
 *
 * exemplar id is -1 if the point is an outlier.
 *
 * \param fname name of the file to store the data in
 * \param data dataset that was clustered
 * \param assignments assignment of points to exemplars
 */
void write_to_disk(
        std::string const&              fname,
        std::vector<Eigen::VectorXd> const& data,
        std::vector<int> const&         assignments
);

/**
 * \brief Writes the clustering results to disk.
 *
 * The outliers, exemplars and assignments are stored in separate files.
 *
 * \param flo the instance of which to store the results
 */
void write_results(FacilityLocationOutliers & flo);

/**
 * \brief Returns the scaled median value of all point to point distances.
 *
 * \param data the points to compute the distances from
 * \param scale the scaling factor
 * \return scaled median point to point distance
 */
double median_cost(
        std::vector<Eigen::VectorXd> const& data,
        std::function<double (Eigen::VectorXd const&, Eigen::VectorXd const&)> dist_function,
        double                          scale
);

/**
 * \brief Returns the scaled median value of all point to point distances.
 *
 * \param data the points to compute the distances from
 * \param scale the scaling factor
 * \return scaled median point to point distance
 */
double median_cost(kobold::GaussianDataset<double> const& data, double scale);

/**
 * \brief Returns the scaled median value of all point to point distances.
 *
 * \param distances the pointwise distances
 * \param scale the scaling factor
 * \return scaled median point to point distance
 */
double median_cost(Eigen::MatrixXd const& distances, double scale);

/**
 * \brief Constructs a similarity matrix based on the negative euclidean distance.
 *
 * \param data the data points to build the matrix for
 * \param cluster_penalty scaling factor for diagonal entries
 * \return similarity matrix
 */
Eigen::MatrixXd build_simmat(
        std::vector<Eigen::VectorXd> const& data,
        double                  cluster_penalty
);

/**
 * \brief Computes the energy or cost of a particular solution.
 *
 * Outliers are indicated by having an assignment value of -1.
 *
 * \param distances the distance between any two points
 * \param exemplar_ids the selected exemplar ids
 * \param assignments assignment of points to exemplars
 * \return cost of the solution
 */
double compute_energy(
        Eigen::MatrixXd const&          distances,
        std::vector<int>                exemplar_ids,
        std::vector<int>                assignments
);


#endif /* __UTIL_H__ */
