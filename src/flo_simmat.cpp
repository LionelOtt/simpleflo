#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>

#include <facility_location_outliers_exact.h>
#include <util.h>


// Forward declarations
struct DataEntry;


/**
 * \brief Parses string represenation of a matrix into an actual matrix.
 *
 * \param dimension the dimensionality of the matrix
 * \param data text representation of the matrix
 * \return matrix object
 */
Eigen::MatrixXd parse_matrix(
        std::string const&              dimension,
        std::string const&              data
);

/**
 * \brief Creates a cost vector based on a distance matrix.
 *
 * \param distances the distance matrix
 * \param scale scaling factor for the cost values
 * \return cost value vector
 */
Eigen::VectorXd generate_cost_vector(
        Eigen::MatrixXd const&          distances,
        double                          scale
);

/**
 * \brief Parses the file into DataEntry objects
 *
 * \param fname data file path
 * \return vector containing DataEntry objects
 */
std::vector<DataEntry> parse_file(std::string const& fname);


/**
 * \brief Represents a single data entry.
 */
struct DataEntry
{
    std::string                         identifier;     //! Data entry identifier
    Eigen::MatrixXd                     distances;      //! Distance matrix
    Eigen::VectorXd                     costs;          //! Facility costs


    /**
     * \brief Creates a new object.
     *
     * \param _identifier string identifier of this entry
     * \param _distances distance matrix of this entry
     */
    DataEntry(
            std::string const&          _identifier,
            Eigen::MatrixXd             _distances
    )   :   identifier(_identifier)
          , distances(_distances)
          , costs(generate_cost_vector(_distances, 2.0))
    {}
};


int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        std::cerr << "Usage: " << argv[0] << " <simmat file>" << std::endl;
        return 1;
    }

    auto data_entries = parse_file(argv[1]);
    auto outlier_counts = std::vector<int>{0, 1, 2, 3};
    for(auto const& entry : data_entries)
    {
        std::cout << "\"" << entry.identifier << "\"";
        for(auto & l : outlier_counts)
        {
            FacilityLocationOutliersExact flo(
                entry.distances,
                entry.costs,
                std::min(l, static_cast<int>(entry.costs.size()))
            );
            flo.iterate(1000);

            std::cout << ",\"" << l << "\""
                      << ",\"" << flo.energy() << "\"";
        }
        std::cout << std::endl;
    }

    return 0;
}


Eigen::MatrixXd parse_matrix(
        std::string const&              dimension,
        std::string const&              data
)
{
    int dim = boost::lexical_cast<int>(dimension);
    Eigen::MatrixXd distances(dim, dim);

    // Parse matrix data
    boost::regex re("\\[([\\d\\. ]+)\\]");
    boost::smatch match;

    int row = 0;
    auto begin = data.begin();
    auto end = data.end();
    while(boost::regex_search(begin, end, match, re))
    {
        begin += match.length();

        auto line = match[1].str();
        boost::trim(line);
        std::vector<std::string> parts;
        boost::split(parts, line, boost::is_any_of(" "), boost::token_compress_on);

        for(size_t i=0; i<parts.size(); ++i)
        {
            distances(row, i) = boost::lexical_cast<double>(parts[i]);
        }
        row++;
    }

    distances += Eigen::MatrixXd(distances.transpose());

    return distances;
}

Eigen::VectorXd generate_cost_vector(
        Eigen::MatrixXd const&          distances,
        double                          scale
)
{
    return Eigen::VectorXd::Constant(
            distances.rows(),
            median_cost(distances, scale)
    );
}

std::vector<DataEntry> parse_file(std::string const& fname)
{
    std::ifstream input(fname);
    std::string line;

    std::vector<std::string> data;
    std::stringstream combined_line;

    while(input.good())
    {
        getline(input, line);
        boost::trim(line);

        if(line.back() == '"')
        {
            combined_line << line;
            data.push_back(combined_line.str());

            combined_line.str("");
            combined_line.clear();
        }
        else
        {
            combined_line << line << " ";
        }
    }

    // Parse each line
    boost::regex re("\"(.*\",\".*)\",\"(\\d+)\",\"(.*)\"");
    boost::smatch matches;

    std::vector<DataEntry> entries;
    for(auto const& entry : data)
    {
        if(boost::regex_match(entry, matches, re))
        {
            try
            {
                auto distances = parse_matrix(matches[2].str(), matches[3].str());
                entries.emplace_back(DataEntry(matches[1].str(), distances));
            }
            catch(boost::bad_lexical_cast const& e)
            {
                std::cout << "Input data invalid" << std::endl;
                std::cout << matches[3].str() << std::endl;
            }
        }
    }

    return entries;
}
