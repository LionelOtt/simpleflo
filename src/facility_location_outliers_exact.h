#ifndef __FACILITY_LOCATION_OUTLIERS_EXACT_H__
#define __FACILITY_LOCATION_OUTLIERS_EXACT_H__


#include <Eigen/Core>


/**
 * \brief Working outlier clustering method based solely on clustering
 * constraint relaxation.
 */
class FacilityLocationOutliersExact
{
    public:
        FacilityLocationOutliersExact(
                Eigen::MatrixXd const&  distances,
                Eigen::VectorXd const&  costs,
                int                     outlier_count
        );

        int iterate(int max_iterations);

        /**
         * \brief Returns the ids of the exemplars.
         *
         * \return ids of the selected exemplars
         */
        std::vector<int> get_exemplars() const;

        /**
         * \brief Returns the ids of the outliers.
         *
         * \return ids of the selected outliers
         */
        std::vector<int> get_outliers() const;

        /**
         * \brief Returns the exemplar id each data point is assigned to.
         *
         * \return assignments of points to exemplar ids
         */
        std::vector<int> get_assignments() const;

        /**
         * \brief Returns the energy of the solution.
         *
         * \return solution energy
         */
        double energy() const;

    private:
        /**
         * \brief Computes the next subgradient.
         *
         * \return new subgradient
         */
        Eigen::VectorXd compute_subgradient();

    private:
        //! Distance between pairs of points
        Eigen::MatrixXd                 m_distances;
        //! Cost of selecting a point as exemplar
        Eigen::VectorXd                 m_costs;
        //! Number of outliers to select
        int                             m_outlier_count;

        //! Storage for the Lagrangean multipliers
        Eigen::VectorXd                 m_lambda;
        //! Current assignment matrix solution
        Eigen::MatrixXd                 m_solution;
        //! Exemplar selections
        Eigen::VectorXd                 m_exemplars;
        //! Outlier selections
        Eigen::VectorXd                 m_outliers;
};


#endif /* __FACILITY_LOCATION_OUTLIERS_EXACT_H__ */
