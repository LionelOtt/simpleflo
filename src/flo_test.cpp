#include <iostream>
#include <random>

#include <Eigen/Core>

#include <kobold/gaussian_dataset.hpp>
#include <kobold/timing.hpp>

#include <facility_location_outliers.h>
#include <util.h>


typedef kobold::GaussianDataset<double> Dataset_t;


int main(int argc, char *argv[])
{
    auto ds = Dataset_t{5, 2};
    ds.generate_uniform(500);
    ds.add_noise(0.1);

    std::vector<Eigen::VectorXd> data = ds.points();
    //std::random_device rng_dev;
    //std::mt19937 gen(rng_dev());
    //std::shuffle(data.begin(), data.end(), gen);

    kobold::Timing t;
    FacilityLocationOutliers ld(
            data,
            euclidean_distance,
            median_cost(ds, 5.0),
            FacilityLocationOutliers::Mode::Count
    );
    ld.set_outlier_count(50);
    int iters = ld.iterate(500, 1.0);
    t.update(iters);

    std::cout << "iters=" << iters
              << " time=" << t.diff()
              << " energy=" << ld.energy()
              << std::endl;
    write_to_disk("/tmp/flo.dat", ds.points(), ld.assignments());

    return 0;
}
