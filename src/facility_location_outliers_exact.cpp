#include <facility_location_outliers_exact.h>


FacilityLocationOutliersExact::FacilityLocationOutliersExact(
        Eigen::MatrixXd const&          distances,
        Eigen::VectorXd const&          costs,
        int                             outlier_count
)
    :   m_distances(distances)
      , m_costs(costs)
      , m_outlier_count(outlier_count)
      , m_lambda(Eigen::VectorXd::Ones(distances.rows()))
      , m_solution(Eigen::MatrixXd::Zero(distances.rows(), distances.rows()))
      , m_exemplars(Eigen::VectorXd::Zero(distances.rows()))
      , m_outliers(Eigen::VectorXd::Zero(distances.rows()))
{}

int FacilityLocationOutliersExact::iterate(int max_iterations)
{
    std::vector<double> epsilon_values;

    bool solution_feasible = false;
    int iter_count = 0;
    double epsilon = 10.0;
    double theta_0 = 1.0;
    double alpha = 0.99;
    while(iter_count < max_iterations && !solution_feasible)
    {
        auto subgradient = compute_subgradient();

        double theta = theta_0 * std::pow(alpha, iter_count);

        Eigen::VectorXd lambda_new = (m_lambda + (theta * subgradient))
            .cwiseMax(Eigen::VectorXd::Zero(m_lambda.size()));

        epsilon = (lambda_new - m_lambda).norm();
        m_lambda = lambda_new;
        epsilon_values.push_back(epsilon);

        iter_count++;

        // Check if we have a feasible solution
        solution_feasible =
            (m_solution.rowwise().sum() == Eigen::VectorXd::Ones(m_solution.rows())) &&
            (m_outliers.sum() == m_outlier_count);
    }

    return iter_count;
}


std::vector<int> FacilityLocationOutliersExact::get_outliers() const
{
    std::vector<int> outlier_ids;
    for(int i=0; i<m_outliers.size(); ++i)
    {
        if(m_outliers[i] > 0.9)
        {
            outlier_ids.push_back(i);
        }
    }

    assert(outlier_ids.size() == static_cast<size_t>(m_outlier_count));

    return outlier_ids;
}

std::vector<int> FacilityLocationOutliersExact::get_exemplars() const
{
    std::vector<int> exemplar_ids;
    for(int i=0; i<m_exemplars.size(); ++i)
    {
        if(m_exemplars[i] > 0.9)
        {
            exemplar_ids.push_back(i);
        }
    }

    return exemplar_ids;
}

std::vector<int> FacilityLocationOutliersExact::get_assignments() const
{
    std::vector<int> assignments;

    auto outliers = get_outliers();

    for(int i=0; i<m_solution.rows(); ++i)
    {
        if(std::find(outliers.begin(), outliers.end(), i) != outliers.end())
        {
            assignments.push_back(-1);
        }
        else
        {
            for(int j=0; j<m_solution.cols(); ++j)
            {
                if(m_solution(i, j) > 0.9)
                {
                    assignments.push_back(j);
                    break;
                }
            }
        }
    }

    return assignments;
}

double FacilityLocationOutliersExact::energy() const
{
    double total_energy = 0.0;
    for(int i=0; i<m_exemplars.size(); ++i)
    {
        if(m_exemplars[i] > 0.9)
        {
            total_energy += m_costs[i];
        }
    }

    auto assignments = get_assignments();
    for(size_t i=0; i<assignments.size(); ++i)
    {
        if(assignments[i] != -1)
        {
            total_energy += m_distances(i, assignments[i]);
        }
    }

    return total_energy;
}

Eigen::VectorXd FacilityLocationOutliersExact::compute_subgradient()
{
    typedef std::pair<int, double> SortPair_t;

    int rows = m_distances.rows();
    int cols = m_distances.cols();

    m_outliers.fill(0.0);
    std::vector<SortPair_t> outlier_weights(rows);

    for(int i=0; i<rows; ++i)
    {
        bool possible_outlier = false;
        for(int j=0; j<cols; ++j)
        {
            if(m_distances(i, j) - m_lambda[i] >= 0.0)
            {
                m_solution(i, j) = 0.0;
                possible_outlier = true;
            }
        }

        if(possible_outlier)
        {
            outlier_weights[i] = {i, m_lambda[i]};
        }
        else
        {
            outlier_weights[i] = {i, 0.0};
        }
    }

    // Sort pairs descending by their lambda values
    std::sort(
            outlier_weights.begin(),
            outlier_weights.end(),
            [](SortPair_t const& a, SortPair_t const& b)
            {
                return a.second > b.second;
            }
    );

    for(int i=0; i<m_outlier_count; ++i)
    {
        m_outliers[outlier_weights[i].first] = 1.0;
    }

    Eigen::VectorXd mu(rows);
    for(int i=0; i<rows; ++i)
    {
        mu[i] = m_costs[i];
    }

    for(int j=0; j<cols; ++j)
    {
        for(int i=0; i<rows; ++i)
        {
            double diff = m_distances(i, j) - m_lambda[i];
            if(diff < 0.0)
            {
                mu[j] += diff;
            }
        }
    }

    for(int j=0; j<cols; ++j)
    {
        if(mu[j] < 0.0)
        {
            m_exemplars[j] = 1.0;
        }
        else
        {
            m_exemplars[j] = 0.0;
        }
    }

    for(int i=0; i<rows; ++i)
    {
        for(int j=0; j<cols; ++j)
        {
            if(m_distances(i, j) - m_lambda[i] < 0.0)
            {
                m_solution(i, j) = m_exemplars[j];
            }
        }
    }


    // Compute the new subgradient
    auto a = Eigen::VectorXd::Ones(m_distances.rows());
    auto b = Eigen::VectorXd(m_solution.rowwise().sum());
    return Eigen::VectorXd(a - b);
}

