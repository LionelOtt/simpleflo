#include <fstream>

#include <util.h>
#include <median_helper.h>


double euclidean_distance(Eigen::VectorXd const& a, Eigen::VectorXd const& b)
{
    return (a - b).norm();
}

double angular_distance(Eigen::VectorXd const& a, Eigen::VectorXd const& b)
{
    double distance = 0.0;

    auto diff = a - b;
    for(int i=0; i<diff.size(); ++i)
    {
        double angle = M_PI - std::abs(std::abs(diff[i]) - M_PI);
        assert(angle >= 0.0 && angle <= 2*M_PI);
        distance += angle;
    }
    
    return distance;
}

void write_to_disk(
        std::string const&              fname,
        std::vector<Eigen::VectorXd> const& data,
        std::vector<int> const&         assignments
)
{
    // Write label and coordinates to disk
    std::ofstream out(fname);
    for(size_t i=0; i<assignments.size(); ++i)
    {
        out << assignments[i] << " " << data[i].transpose() << std::endl;
    }
}

void write_results(FacilityLocationOutliers & flo)
{
    // Store the results
    std::ofstream out_outliers("/tmp/outliers.dat");
    for(auto & v : flo.outlier_ids())
    {
        out_outliers << v << std::endl;
    }

    std::ofstream out_exemplars("/tmp/exemplars.dat");
    for(auto & v : flo.exemplar_ids())
    {
        out_exemplars << v << std::endl;
    }

    std::ofstream out_assignments("/tmp/assignments.dat");
    auto assignments = flo.assignments();
    for(size_t i=0; i<assignments.size(); ++i)
    {
        out_assignments << i << " " << assignments[i] << std::endl;
    }
}

double median_cost(
        std::vector<Eigen::VectorXd> const& data,
        std::function<double (Eigen::VectorXd const&, Eigen::VectorXd const&)> dist_function,
        double                          scale
)
{
    MedianHelper mh(scale, 0.05);
    for(size_t i=0; i<data.size(); ++i)
    {
        for(size_t j=0; j<data.size(); ++j)
        {
            if(i != j)
            {
                mh.add(dist_function(data[i], data[j]));
            }
        }
    }

    return mh.value();
}

double median_cost(kobold::GaussianDataset<double> const& data, double scale)
{
    MedianHelper mh(scale, 0.05);
    for(size_t i=0; i<data.points().size(); ++i)
    {
        for(size_t j=0; j<data.points().size(); ++j)
        {
            if(i != j)
            {
                mh.add((data.points()[i] - data.points()[j]).norm());
            }
        }
    }

    return mh.value();
}

double median_cost(Eigen::MatrixXd const& distances, double scale)
{
    MedianHelper mh(scale, 0.05);
    for(int i=0; i<distances.rows(); ++i)
    {
        for(int j=0; j<distances.cols(); ++j)
        {
            if(i != j)
            {
                mh.add(distances(i, j));
            }
        }
    }

    return mh.value();
}

Eigen::MatrixXd build_simmat(
        std::vector<Eigen::VectorXd> const& data,
        double                          cluster_penalty
)
{
    Eigen::MatrixXd mat(data.size(), data.size());
    MedianHelper mh(cluster_penalty, 0.05);
    for(size_t i=0; i<data.size(); ++i)
    {
        for(size_t j=0; j<data.size(); ++j)
        {
            if(i != j)
            {
                mat(i, j) = -(data[i] - data[j]).norm();
                mh.add(mat(i, j));
            }
        }
    }

    for(size_t i=0; i<data.size(); ++i)
    {
        mat(i, i) = mh.value();
    }

    return mat;
}

double compute_energy(
        Eigen::MatrixXd const&          distances,
        std::vector<int>                exemplar_ids,
        std::vector<int>                assignments
)
{
    double energy = 0.0;

    for(auto & id : exemplar_ids)
    {
        energy += distances(id, id);
    }

    for(size_t i=0; i<assignments.size(); ++i)
    {
        if(assignments[i] != -1)
        {
            energy += distances(i, assignments[i]);
        }
    }

    return -energy;
}
