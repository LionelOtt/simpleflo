#ifndef __FACILITY_LOCATION_OUTLIERS_H__
#define __FACILITY_LOCATION_OUTLIERS_H__


#include <functional>
#include <unordered_set>

#include <Eigen/Core>
#include <Eigen/SparseCore>


/**
 * \brief Lagrangean duality based facility location with outlier detection.
 */
class FacilityLocationOutliers
{
    typedef std::vector<Eigen::VectorXd> Dataset_t;
    typedef std::function<double (Eigen::VectorXd const&, Eigen::VectorXd const&)> CostFunction_t;
    typedef Eigen::SparseMatrix<double, Eigen::RowMajor> SparseMatrix_t;

    public:
        enum class Mode
        {
            Count,
            Threshold
        };

    public:
        /**
         * \brief Creates a new instance.
         *
         * \param data the dataset to compute the solution for
         * \param cost_function the cost function used to compute the distance
         *      between two points
         * \param outlier_count number of outliers to select
         * \param facility_cost the cost of creating a new facility
         */
        FacilityLocationOutliers(
                Dataset_t const&        data,
                CostFunction_t          cost_function,
                double                  facility_cost,
                Mode                    outlier_mode
        );

        /**
         * \brief Iterates the problem until convergence or timeout.
         *
         * \param max_iterations maximum number of iterations to run
         */
        int iterate(int max_iterations, double theta_0=1.0);

        /**
         * \brief Sets the number of outliers to find.
         *
         * \param count number of outliers to find
         */
        void set_outlier_count(int count);

        /**
         * \brief Sets the outlier weight threshold.
         *
         * \param threshold the weight threshold to use to determine the
         *      outliers
         */
        void set_outlier_threshold(double threshold);

        /**
         * \brief Sets the outlier selection mode.
         *
         * \param mode the mode according to which outliers are selected
         */
        void set_outlier_mode(Mode mode);

        /**
         * \brief Returns the ids of the exemplars.
         *
         * \return ids of the selected exemplars
         */
        std::vector<int> exemplar_ids() const;

        /**
         * \brief Returns the ids of the outliers.
         *
         * \return ids of the selected outliers
         */
        std::vector<int> outlier_ids() const;

        /**
         * \brief Returns the exemplar id each data point is assigned to.
         *
         * \return assignments of points to exemplar ids
         */
        std::vector<int> assignments();

        /**
         * \brief Returns the energy of the current solution.
         *
         * \return current solution's energy
         */
        double energy();

    private:
        /**
         * \brief Returns a gradient to improve the current solution.
         *
         * This method computes an assignment solution and derives a new
         * subgradient from this solution.
         *
         * \return subgradient vector to improve the solution
         */
        Eigen::VectorXd compute_subgradient();

        /**
         * \brief Selects the outliers according to the criteria set.
         *
         * \oaram outlier_weights the current outlier weights
         */
        void select_outliers();

        int outlier_count() const;
        void validate();

    private:
        //! Cost of creating a new cluster
        double                          m_facility_cost;
        //! Data points to be clustered
        std::vector<Eigen::VectorXd>    m_points;
        //! Cost function
        CostFunction_t                  m_cost_function;
        //! Number of outliers
        int                             m_outlier_count;
        //! Outlier weight threshold
        double                          m_outlier_threshold;
        //! Outlier selection mode
        Mode                            m_outlier_mode;

        //! Subgradient storage
        Eigen::VectorXd                 m_subgradient;
        //! Outlier indicator matrix
        Eigen::VectorXd                 m_outlier_indicator;
        //! Outlier index set
        std::unordered_set<int>         m_outlier_set;
        //! Exemplar indicator vector
        std::vector<bool>               m_exemplar_indicator;
        //! Current solution to the clustering problem
        SparseMatrix_t                  m_solution;
        //! Lambda values
        Eigen::VectorXd                 m_lambda;
};

#endif /* __FACILITY_LOCATION_OUTLIERS_H__ */
