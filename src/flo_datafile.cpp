#include <functional>
#include <iostream>
#include <unordered_map>

#include <kobold/debug.h>
#include <kobold/files.h>

#include <facility_location_outliers.h>
#include <util.h>


int main(int argc, char *argv[])
{
    if(argc != 3)
    {
        std::cerr << "Usage: " << argv[0] << " <data file> <distance method>" << std::endl;
        return 1;
    }

    using namespace std::placeholders;

    std::string data_file(argv[1]);
    std::string distance_method(argv[2]);

    auto data = kobold::read<double>(data_file);

    if(distance_method == "angular")
    {
        FacilityLocationOutliers flo(
                data,
                angular_distance,
                median_cost(data, angular_distance, 5.0),
                FacilityLocationOutliers::Mode::Threshold
        );
        flo.set_outlier_threshold(0.1);
        flo.iterate(1000);

        write_results(flo);
    }
    else if(distance_method == "euclidean")
    {
        FacilityLocationOutliers flo(
                data,
                euclidean_distance,
                median_cost(data, euclidean_distance, 15.0),
                FacilityLocationOutliers::Mode::Count
        );
        flo.set_outlier_count(100);
        flo.iterate(1000);

        write_results(flo);
    }

    return 0;
}
