#include <fstream>
#include <iostream>
#include <unordered_set>

#include <kobold/timing.hpp>

#include <facility_location_outliers.h>


FacilityLocationOutliers::FacilityLocationOutliers(
        Dataset_t const&        data,
        CostFunction_t          cost_function,
        double                  facility_cost,
        Mode                    outlier_mode
)   :   m_facility_cost(facility_cost)
      , m_points(data)
      , m_cost_function(cost_function)
      , m_outlier_count(0)
      , m_outlier_threshold(0.0)
      , m_outlier_mode(outlier_mode)
      , m_subgradient(m_points.size())
      , m_outlier_indicator(Eigen::VectorXd::Zero(m_points.size()))
      , m_exemplar_indicator(m_points.size())
      , m_solution(m_points.size(), m_points.size())
      , m_lambda(Eigen::VectorXd::Random(m_points.size()))
{}

int FacilityLocationOutliers::iterate(int max_iterations, double theta_0)
{
    kobold::Timing t;

    // Initialize data structures
    static std::vector<double> epsilon_values;
    static std::vector<double> lambda_magnitudes;


    int iter_count = 1;
    double epsilon = 10.0;
    double min_epsilon = 0.25;
    double alpha = 0.95;
    bool running = true;

    // Iterate until convergence
    while(running)
    {
        auto subgradient = compute_subgradient();

        //validate();

        double theta = theta_0 * std::pow(alpha, iter_count);

        Eigen::VectorXd lambda_new = (m_lambda + (theta * subgradient))
            .cwiseMax(Eigen::VectorXd::Zero(m_lambda.size()));

        epsilon = (lambda_new - m_lambda).norm();
        epsilon_values.push_back(epsilon);

        std::swap(m_lambda, lambda_new);
        lambda_magnitudes.push_back(m_lambda.norm());


        if(iter_count > max_iterations)
        {
            running = false;
        }
        if(epsilon < min_epsilon)
        {
            running = false;
        }
        iter_count++;
    }

    t.update(iter_count);


    // Dump epsilon deltas and lambda magnitudes
    std::ofstream eps_out("/tmp/epsilon.dat");
    for(size_t i=0; i<epsilon_values.size(); ++i)
    {
        eps_out << epsilon_values[i] << " " << lambda_magnitudes[i] << std::endl;
    }

    return iter_count;
}

void FacilityLocationOutliers::set_outlier_count(int count)
{
    m_outlier_count = count;
}

void FacilityLocationOutliers::set_outlier_threshold(double threshold)
{
    m_outlier_threshold = threshold;
}

void FacilityLocationOutliers::set_outlier_mode(Mode mode)
{
    m_outlier_mode = mode;
}

Eigen::VectorXd FacilityLocationOutliers::compute_subgradient()
{
    int dim = m_points.size();
    double alpha = 0.99;
    std::vector<std::pair<int, int>> non_zero_indices;

    Eigen::VectorXd mu(dim);
    SparseMatrix_t new_solution(dim, dim);
    new_solution.reserve(m_solution.nonZeros());


    // Select outlier points
    select_outliers();

    // Compute exemplar and outlier candidates
    for(int j=0; j<dim; ++j)
    {
        mu[j] = m_facility_cost;
        for(int i=0; i<dim; ++i)
        {
            double diff = m_cost_function(m_points[i], m_points[j]) - m_lambda[i];
            if(diff < 0.0)
            {
                mu[j] += diff;
                non_zero_indices.push_back({i, j});
            }
        }
    }

    for(int i=0; i<dim; ++i)
    {
        m_exemplar_indicator[i] = mu[i] < 0.0 ? true : false;
    }

    // Update the solution
    for(auto & coord : non_zero_indices)
    {
        if(m_exemplar_indicator[coord.second])
        {
            new_solution.insert(coord.first, coord.second) = 1.0;
        }
    }

    m_solution = (alpha * new_solution) + (1.0 - alpha) * m_solution;

    // Compute new subgradient
    Eigen::VectorXd subgradient = Eigen::VectorXd::Zero(dim);
    for(int i=0; i<dim; ++i)
    {
        subgradient[i] = m_solution.row(i).sum();
    }
    subgradient = Eigen::VectorXd::Ones(dim) - subgradient;

    return subgradient;
}

void FacilityLocationOutliers::select_outliers()
{
    typedef std::pair<int, double> SortPair_t;

    // Create index, value pairs
    std::vector<SortPair_t> weights;
    weights.reserve(m_lambda.size());
    for(int i=0; i<m_lambda.size(); ++i)
    {
        //// Exemplars cannot be choosen as outliers
        //if(m_exemplar_indicator[i])
        //{
        //    weights.push_back({i, 0.0});
        //}
        //else
        //{
        //    weights.push_back({i, m_lambda[i]});
        //}
        weights.push_back({i, m_lambda[i]});
    }

    // Sort outlier candidates descending according to their lambda values
    std::sort(
            weights.begin(),
            weights.end(),
            [](SortPair_t const& a, SortPair_t const& b)
            {
                return a.second > b.second;
            }
    );

    // Select outliers according to the active policy
    m_outlier_indicator.setZero();
    m_outlier_set.clear();
    if(m_outlier_mode == Mode::Count)
    {
        for(int i=0; i<m_outlier_count; ++i)
        {
            m_outlier_indicator[weights[i].first] = 1.0;
            m_outlier_set.insert(weights[i].first);
        }
    }
    else if(m_outlier_mode == Mode::Threshold)
    {
        double threshold = m_outlier_threshold * m_lambda.sum();
        int index = 0;
        while(threshold > 0.0)
        {
            m_outlier_indicator[weights[index].first] = 1.0;
            m_outlier_set.insert(weights[index].first);
            threshold -= weights[index].second;
            index++;
        }
    }
}

double FacilityLocationOutliers::energy()
{
    double total_energy = m_facility_cost * exemplar_ids().size();

    auto point_assignments = assignments();
    for(size_t i=0; i<point_assignments.size(); ++i)
    {
        if(point_assignments[i] != -1)
        {
            total_energy += m_cost_function(
                    m_points[i],
                    m_points[point_assignments[i]]
            );
        }
    }

    return total_energy;
}

std::vector<int> FacilityLocationOutliers::exemplar_ids() const
{
    std::vector<int> data;

    std::vector<double> max_values(m_solution.rows(), 0.0);
    for(int i=0; i<m_solution.outerSize(); ++i)
    {
        for(SparseMatrix_t::InnerIterator itr(m_solution, i); itr; ++itr)
        {
            if(max_values[itr.col()] < itr.value())
            {
                max_values[itr.col()] = itr.value();
            }
        }
    }

    for(size_t i=0; i<max_values.size(); ++i)
    {
        if(max_values[i] > 0.9)
        {
            data.push_back(i);
        }
    }

    return data;
}

std::vector<int> FacilityLocationOutliers::outlier_ids() const
{
    std::vector<int> data;

    for(int i=0; i<m_outlier_indicator.size(); ++i)
    {
        if(m_outlier_indicator[i] > 0.9)
        {
            data.push_back(i);
        }
    }

    return data;
}

std::vector<int> FacilityLocationOutliers::assignments()
{
    std::vector<int> data;

    auto exemplars = exemplar_ids();
    std::unordered_set<int> exemplar_set(exemplars.begin(), exemplars.end());

    for(int i=0; i<m_solution.outerSize(); ++i)
    {
        std::pair<int, double> max_entry = {-1, 0.0};
        if(m_outlier_indicator[i] > 0.9)
        {
            max_entry.first = -1;
        }
        // If we can't assign the point turn it into it's own exemplar
        else
        {
            for(SparseMatrix_t::InnerIterator itr(m_solution, i); itr; ++itr)
            {
                if(max_entry.second < itr.value())
                {
                    max_entry = {itr.col(), itr.value()};
                }
            }
            if(max_entry.first == -1)
            {
                max_entry = {i, 0.0};
                m_exemplar_indicator[i] = true;
                m_solution.insert(i, i) = 1.0;
            }
        }
        data.push_back(max_entry.first);
    }

    return data;
}

int FacilityLocationOutliers::outlier_count() const
{
    int count = 0;

    for(int i=0; i<m_solution.outerSize(); ++i)
    {
        std::pair<int, double> max_entry = {-1, 0.0};
        if(m_outlier_indicator[i] > 0.9)
        {
            count++;
        }
        else
        {
            for(SparseMatrix_t::InnerIterator itr(m_solution, i); itr; ++itr)
            {
                if(max_entry.second < itr.value())
                {
                    max_entry = {itr.col(), itr.value()};
                }
            }
            if(max_entry.first == -1)
            {
                count++;
            }
        }
    }

    std::cout << count << std::endl;
    return count;
}

void FacilityLocationOutliers::validate()
{
    assert(m_outlier_set.size() == static_cast<size_t>(m_outlier_count));

    auto ptca = assignments();
    auto exid = exemplar_ids();

    for(size_t i=0; i<ptca.size(); ++i)
    {
        if(i == static_cast<size_t>(ptca[i]))
        {
            assert(std::find(exid.begin(), exid.end(), i) != exid.end());
        }
        if(ptca[i] == -1)
        {
            assert(m_outlier_set.find(i) == m_outlier_set.end());
        }
    }
}
