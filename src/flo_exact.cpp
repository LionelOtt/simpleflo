#include <iostream>

#include <Eigen/Core>

#include <kobold/gaussian_dataset.hpp>
#include <kobold/timing.hpp>

#include <facility_location_outliers_exact.h>
#include <util.h>


typedef kobold::GaussianDataset<double> Dataset_t;


Eigen::MatrixXd generate_distance_matrix(
        std::vector<Eigen::VectorXd> const& points
)
{
    Eigen::MatrixXd dist(points.size(), points.size());

    for(size_t i=0; i<points.size(); ++i)
    {
        for(size_t j=0; j<points.size(); ++j)
        {
            dist(i, j) = (points[i] - points[j]).norm();
        }
    }

    return dist;
}

Eigen::VectorXd generate_cost_vector(
        Dataset_t const&                dataset,
        double                          scale
)
{
    return Eigen::VectorXd::Constant(
            dataset.points().size(),
            median_cost(dataset, scale)
    );
}


int main(int argc, char *argv[])
{
    auto ds = Dataset_t{5, 2};
    ds.generate_uniform(100);
    ds.add_noise(0.1);

    auto distances = generate_distance_matrix(ds.points());
    auto costs = generate_cost_vector(ds, 5.0);

    kobold::Timing t;
    FacilityLocationOutliersExact floe(
        distances,
        costs,
        static_cast<int>(0.1 * 100)
    );

    int iters = floe.iterate(1000);
    t.update(iters);

    std::cout << "iters=" << iters
              << " exemplars=" << floe.get_exemplars().size()
              << " outliers=" << floe.get_outliers().size()
              << " time=" << t.diff()
              << std::endl;

    write_to_disk("/tmp/flo_exact.dat", ds.points(), floe.get_assignments());

    return 0;
}
