cmake_minimum_required( VERSION 2.8 )

# +-----------------------------------------------------------------------------
# | Basic settings
# +-----------------------------------------------------------------------------

project( "Lagrange Duality Clustering" )

# Set output directories for libraries and executables
set( BASE_DIR ${CMAKE_SOURCE_DIR} )
set( CMAKE_LIBRARY_OUTPUT_DIRECTORY ${BASE_DIR}/lib )
set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${BASE_DIR}/bin )

# Cmake module search locations
list( APPEND CMAKE_MODULE_PATH ${BASE_DIR}/cmake )

option( BUILD_CTAGS "Build ctag file?" FALSE )


# +-----------------------------------------------------------------------------
# | Library search and setup
# +-----------------------------------------------------------------------------

# Build shared libraries by default
set( BUILD_SHARED_LIBS True )


# Search for required libraries
set( Boost_USE_MULTITHREADED True )
#set( Boost_ADDITIONAL_VERSIONS "1.50" "1.50.0" )
find_package( Boost REQUIRED COMPONENTS filesystem regex program_options system )
find_package( Eigen REQUIRED )
find_package( Kobold REQUIRED )

# Use local include and library directories
include_directories(
    ${BASE_DIR}/src
    ${Boost_INCLUDE_DIRS}
    ${EIGEN_INCLUDE_DIRS}
    ${KOBOLD_INCLUDE_DIRS}
)
link_directories(
    ${BASE_DIR}/lib
)


# +-----------------------------------------------------------------------------
# | Custom targets and macros
# +-----------------------------------------------------------------------------

# Custom target that builds ctags for the source code
if( BUILD_CTAGS )
    add_custom_target(
        generate_ctags ALL
        ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .
        WORKING_DIRECTORY ${BASE_DIR}/src
        COMMENT "Regenerating ctags file"
    )
endif()


# +-----------------------------------------------------------------------------
# | Compiler settings
# +-----------------------------------------------------------------------------

add_definitions( ${PCL_DEFINITIONS} )
if( CMAKE_COMPILER_IS_GNUCXX )
    set( CMAKE_CXX_FLAGS "-std=c++0x -Wall" )
endif()


# +-----------------------------------------------------------------------------
# | Compile code
# +-----------------------------------------------------------------------------

# Add subfolders
add_subdirectory( src )
