#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import sys


def parse_file(fname):
    assignments = []
    outliers = []
    exemplars = []

    for i, line in enumerate(open(fname).readlines()):
        try:
            arr = line.split()
            label = int(arr[0])

            if label == -1:
                outliers.append([float(v) for v in arr[1:]])
            else:
                assignments.append([label] + [float(v) for v in arr[1:]])

            if label == i:
                exemplars.append(assignments[-1])
        except ValueError:
            continue
    return assignments, outliers, exemplars


def main():
    assignments, outliers, exemplars = parse_file(sys.argv[1])

    # Assignments
    plt.scatter(
            [v[1] for v in assignments],
            [v[2] for v in assignments],
            c=[v[0] for v in assignments],
            cmap=plt.get_cmap("Paired")
    )
    # Exemplars
    plt.plot(
            [v[1] for v in exemplars],
            [v[2] for v in exemplars],
            "or",
            markersize=10.0,
            markeredgecolor="k",
            markeredgewidth=1.0,
            zorder=1
    )
    # Outliers
    plt.plot(
            [v[0] for v in outliers],
            [v[1] for v in outliers],
            "sb"
    )
    plt.show()



if __name__ == "__main__":
    sys.exit(main())
